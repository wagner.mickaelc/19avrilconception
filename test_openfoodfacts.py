import pytest
import json

from main import isVegan


def test_rozana_isVegan_true():
    with open('rozana.json', 'r') as json_file:
        rozana_data = json.load(json_file)
    assert isVegan(rozana_data) == True


def test_brioche_isVegan_false():
    with open('brioche.json', 'r') as json_file:
        brioche_data = json.load(json_file)
    assert isVegan(brioche_data) == False
