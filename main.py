import requests
import json

from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/rozana")
def read_root():
    requete = requests.get(
        "https://world.openfoodfacts.org/api/v0/product/3468570116601.json")
    with open('rozana.json', 'w') as json_file:
        json.dump(requete.json(), json_file)
    return(requete.json())


@app.get("/brioche")
def read_root():
    requete = requests.get(
        "https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
    with open('brioche.json', 'w') as json_file:
        json.dump(requete.json(), json_file)
    return(requete.json())


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


def isVegan(requestAsJson):
    print(requestAsJson)
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
            if ingredient["vegan"] == 'no' or ingredient["vegan"] == 'maybe':
                return False
    return True
